//
//  VisualConfigurer.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include <stdlib.h>
#include"ScreenConfiguration.h"


class VisualConfigurer {
private:
    const ScreenConfigurationModel* screenConfiguration;
public:
    VisualConfigurer(ScreenConfigurationModel* model);
    ~VisualConfigurer();
    const ScreenConfigurationModel* getScreenConfiguration();
    void printScreenConfiguration();
};
