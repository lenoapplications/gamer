//
//  Drawer.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include"ScreenConfiguration.h"
#include"Canvas.h"
#include"VisualConfigurer.h"
#include "ObjectsEnvironment.h"
#include <string>
#include <map>
#include<iostream>

class Drawer {
private:
    VisualConfigurer* configurer;
    Canvas* canvas;
    ObjectsEnvironment* objectsEnvironment;
public:
    Drawer(){};
    Drawer(VisualConfigurer* model);
    ~Drawer();
    void drawObjects(ObjectsEnvironment* objectsEnvironment);
    void eraseDrawings();
    void draw();
};

