//
//  ScreenConfiguration.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#ifndef ScreenConfiguration_h
#define ScreenConfiguration_h


class ScreenConfigurationModel {
public:
    const int SCREEN_WIDTH_START;
    const int SCREEN_WIDTH_END;
    const int SCREEN_HEIGHT_START;
    const int SCREEN_HEIGHT_END;
    ScreenConfigurationModel(int wStart, int wEnd, int hStart, int hEnd) :SCREEN_WIDTH_START(wStart), SCREEN_WIDTH_END(wEnd), SCREEN_HEIGHT_START(hStart), SCREEN_HEIGHT_END(hEnd) {}
    virtual ~ScreenConfigurationModel() {};
    
    
};


#endif /* ScreenConfiguration_h */
