//
//  Canvas.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include "ScreenConfiguration.h"
#include "ObjectModel.h"
#include <string>
#include <vector>
using namespace std;




class Canvas{
private:
    vector < vector<char> > canvas;
    
    vector<char> setupLineBoundaries(const int* widthStart,const int* widthEnd,bool marginLine = false);
public:
    Canvas(const ScreenConfigurationModel* model);
    ~Canvas() {};
    void drawObject(ObjectModel* objectModel);
    void refreshCanvas();
    
    vector<vector<char>>* getCanvas();

    
};

