//
//  Engine.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 26/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include <stdio.h>
#include "Drawer.h"
#include "ScreenConfiguration.h"
#include "VisualConfigurer.h"
#include "KeyboardListener.h"
#include "InputProcessor.h"
#include "ObjectsEnvironment.h"
#include "ObjectUpdater.h"
#include <thread>
#include <chrono>
#include <time.h>
#include <iostream>



class Engine{
protected:
    ObjectsEnvironment* objectsEnvironment;    
private:
    bool ENGINE_RUNNING = false;
    const float TIME_TO_PASS_FOR_RENDER = 0.05;
    
    Drawer* drawer;
    KeyboardListener* keyboardListener;
    InputProcessor* inputProcessor;
    ObjectUpdater* objectUpdater;
    
    void setGameRunningStatus(bool status);
    void setKeyboardListener();
    bool getPassedTime(float beginTime);
    void resetBeginTime(clock_t &beginRef);
    void draw();
    void gameLoop();
public:
    Engine();
    ~Engine();
    void setupDrawer(ScreenConfigurationModel* visualConfigurer);
    void setObjectUpdater(ObjectUpdater* objectUpdater);
    void setInputProcessor(InputProcessor* inputProcessor);
    void startEngine();
    ObjectsEnvironment* getObjectEnvironment();
    
};
