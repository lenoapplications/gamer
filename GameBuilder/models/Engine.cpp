//
//  Engine.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 26/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include "Engine.h"



Engine::Engine(): objectsEnvironment(new ObjectsEnvironment()){
}
Engine::~Engine(){
    keyboardListener->setTerminalToBlock();
    delete inputProcessor;
    delete keyboardListener;
    delete drawer;
    delete objectsEnvironment;
    delete objectUpdater;
    
    ENGINE_RUNNING = false;
}


void Engine::setGameRunningStatus(bool status){
    ENGINE_RUNNING = status;
}
void Engine::setKeyboardListener(){
    keyboardListener = new KeyboardListener(inputProcessor,&ENGINE_RUNNING);
    keyboardListener->initTerminalToUnblock();
    keyboardListener->listenForKeys(objectsEnvironment);
}
void Engine::setObjectUpdater(ObjectUpdater *objectUpdater){
    this->objectUpdater = objectUpdater;
}
void Engine::setInputProcessor(InputProcessor* inputProcessor){
    this->inputProcessor = inputProcessor;
}
ObjectsEnvironment* Engine::getObjectEnvironment(){
    return objectsEnvironment;
}
bool Engine::getPassedTime(float beginTime){
    clock_t endTime = clock();
    float difference = endTime - beginTime;
    return ( (float) (difference / CLOCKS_PER_SEC) >= TIME_TO_PASS_FOR_RENDER ) ? true : false;
}
void Engine::resetBeginTime(clock_t &beginRef){
    beginRef = clock();
}


void Engine::setupDrawer(ScreenConfigurationModel *screenConfiguration){
    VisualConfigurer* visualConfigurer = new VisualConfigurer{screenConfiguration};
    drawer = new Drawer(visualConfigurer);
}
void Engine::draw(){
    system("clear");
    drawer->drawObjects(objectsEnvironment);
    drawer->draw();
    drawer->eraseDrawings();
}
void Engine::gameLoop(){
    clock_t begin = clock();
    while(ENGINE_RUNNING){
        if(getPassedTime(begin)){
            objectUpdater->objectUpdate(objectsEnvironment);
            draw();
            resetBeginTime(begin);
        }
    }
}



void Engine::startEngine(){
    setGameRunningStatus(true);
    setKeyboardListener();
    gameLoop();
}
