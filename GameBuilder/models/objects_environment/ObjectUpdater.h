//
//  ObjectUpdater.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 08/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include "ObjectsEnvironment.h"

class ObjectUpdater{
private:
    
public:
    ObjectUpdater(){};
    virtual ~ObjectUpdater(){};
    virtual void objectUpdate(ObjectsEnvironment* objectsEnv) = 0;
};
