//
//  ObjectsEnvironment.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 04/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include <vector>
#include <map>
#include <string>
#include "ObjectModel.h"


class ObjectsEnvironment{
private:
    std::map<std::string,ObjectModel*> objects{};
public:
    ObjectsEnvironment(){};
    ~ObjectsEnvironment();
    void addObject(std::string nameOfObject,ObjectModel* objectModel);
    void removeObject(std::string objectName);
    int numberOfObjects();
    ObjectModel* getObjectModel(std::string nameOfObject);
    std::map<std::string,ObjectModel*>& getObjects();
};
