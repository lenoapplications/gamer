//
//  ObjectModel.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 04/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include <string>



class ObjectModel{
private:
    int rowPosition;
    int columnPosition;
    std::string objectDraw;
public:
    ObjectModel(int x,int y):rowPosition(x),columnPosition(y){};
    ~ObjectModel(){};
    void setDraw(std::string draw);
    std::string getDraw();
    void setNewPosition(int x,int y);
    int getRowPosition();
    int getColumnPosition();
    
};
