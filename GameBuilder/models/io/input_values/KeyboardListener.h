//
//  KeyboardListener.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 26/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <thread>
#include "InputProcessor.h"
#include "ObjectsEnvironment.h"

class KeyboardListener{
    
private:
    struct termios old_tio, new_tio;
    bool* ENGINE_RUNNING;
    InputProcessor* inputProcessor;
    unsigned char c;
    void listenLoop(ObjectsEnvironment* objectsEnv);
public:
    KeyboardListener(InputProcessor* inputHolder,bool* engineRunning): inputProcessor(inputHolder), ENGINE_RUNNING(engineRunning){};
    ~KeyboardListener(){};
    
    void initTerminalToUnblock();
    void setTerminalToBlock();
    void listenForKeys(ObjectsEnvironment* objectsEnv);
    
};
