//
//  InputValueHolder.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 30/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//


#pragma once
#include "Drawer.h"
#include "ObjectsEnvironment.h"


class InputProcessor{
private:

public:
    InputProcessor(){};
    virtual ~InputProcessor(){};
    virtual void keyPressedProcessor(char value,ObjectsEnvironment* objectsEnv) = 0;
};
