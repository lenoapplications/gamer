//
//  VisualConfigurer.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include <stdlib.h>
#include <iostream>
#include "VisualConfigurer.h"


VisualConfigurer::VisualConfigurer(ScreenConfigurationModel* model) : screenConfiguration(model) {
    
}

VisualConfigurer::~VisualConfigurer() {
    std::cout << "deleteing screenConfiguration" << std::endl;
    delete screenConfiguration;
}

const ScreenConfigurationModel* VisualConfigurer::getScreenConfiguration() {
    return screenConfiguration;
}

void VisualConfigurer::printScreenConfiguration() {
    std::cout << "WIDTH START POINT : " << screenConfiguration->SCREEN_WIDTH_START << "\n" << "WIDTH END POINT"
    << screenConfiguration->SCREEN_WIDTH_END << "\n" << "HEIGHT START POINT " << screenConfiguration->SCREEN_HEIGHT_START << "\n" << "HEIGHT END POINT " << screenConfiguration->SCREEN_HEIGHT_END << std::endl;
}
