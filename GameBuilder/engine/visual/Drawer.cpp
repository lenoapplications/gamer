//
//  Drawer.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//
#include<iostream>
#include"Drawer.h"



Drawer::Drawer(VisualConfigurer* model) :configurer(model), canvas(new Canvas{ model->getScreenConfiguration() }){
}
Drawer::~Drawer() {
    std::cout << "deleting configurer and canvas" << std::endl;
    delete configurer;
    delete canvas;
}

void Drawer::drawObjects(ObjectsEnvironment* objectsEnvironment) {
    //OVO TREBA MAKNUTI I MOZDA STAVITI U CANVAS JER TAMO VADIM PRVE KORDINATE!
//    const ScreenConfigurationModel* screenConfiguration = configurer->getScreenConfiguration();
//
//    int widthRange = screenConfiguration->SCREEN_WIDTH_END - screenConfiguration->SCREEN_WIDTH_START;
//    int heightRange = screenConfiguration->SCREEN_HEIGHT_END - screenConfiguration->SCREEN_HEIGHT_START;
//
//    bool widthCondition = (x >= 0 && x < widthRange);
//    bool heightCondtion = (y >= 0 && y < heightRange);
//
//    if (widthCondition && heightCondtion) {
//
//    }
    std::map<std::string,ObjectModel*>& objects = objectsEnvironment->getObjects();
    for (auto const& it : objects){
        ObjectModel* model = it.second;
        canvas->drawObject(model);
    }
}
void Drawer::eraseDrawings() {
    canvas->refreshCanvas();
}


void Drawer::draw() {
    vector<vector<char>>* canvasChars = canvas->getCanvas();
    
    int emptyHeightSpaceIndex = 0 - configurer->getScreenConfiguration()->SCREEN_HEIGHT_START;
    int emptyWidthSpaceIndex = 0 - configurer->getScreenConfiguration()->SCREEN_WIDTH_START;
    int numberOfRows = canvasChars->size();
    
    for (int i = emptyHeightSpaceIndex; i < numberOfRows; i++) {
        if (i >= 0) {
            vector<char>* row = &canvasChars->at(i);
            int rowSize = row->size();
            
            for (int j = emptyWidthSpaceIndex; j < rowSize; j++) {
                if (j >= 0) {
                    std::cout << row->at(j);
                }
                else {
                    std::cout << ' ';
                }
            }
        }
        std::cout << endl;
    }
}
