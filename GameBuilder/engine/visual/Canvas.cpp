//
//  Canvas.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include <stdio.h>
#include "Canvas.h"
#include "ScreenConfiguration.h"


Canvas::Canvas(const ScreenConfigurationModel* model) {
    for (int i = 0; i < model->SCREEN_HEIGHT_END; i++) {
        if (i >= model->SCREEN_HEIGHT_START) {
            
            if (i == model->SCREEN_HEIGHT_START || i == model->SCREEN_HEIGHT_END - 1) {
                canvas.push_back(setupLineBoundaries(&model->SCREEN_WIDTH_START,&model->SCREEN_WIDTH_END, true));
            }
            else {
                canvas.push_back(setupLineBoundaries(&model->SCREEN_WIDTH_START,&model->SCREEN_WIDTH_END));
            }
        }
    }
}

vector<char> Canvas::setupLineBoundaries(const int* widthStart,const int* widthEnd,bool marginLine ) {
    vector<char> chars;
    
    if (marginLine) {
        for (int i = 0; i < *widthEnd+1; i++) {
            if (i >= *widthStart) {
                chars.push_back('-');
            }
        }
    }
    else {
        //MOZDA BI MOGAO CAK I MAKNUTI OVAJ DIO KAD TEK PUSHA KAD I BUDE VECI ILI JEDNAK WIDTH STARTU,MOZDA MOGU NAPRAVITI DA ODMAH POCNEMO PUSHAT
        bool drawFirst = true;
        for (int i = 0; i < *widthEnd+1; i++) {
            if (i >= *widthStart) {
                if (drawFirst || i == *widthEnd) {
                    chars.push_back('|');
                    drawFirst = false;
                }
                else {
                    chars.push_back(' ');
                }
            }
        }
    }
    return chars;
}
void Canvas::drawObject(ObjectModel* objectModel){
    int rowPosition = objectModel->getRowPosition();
    int columnPosition = objectModel->getColumnPosition();
    string objectString = objectModel->getDraw();
    
    for (char c : objectString){
        switch (c) {
            case '\n':
                ++rowPosition;
                --columnPosition;
                break;
            case ' ':
                ++columnPosition;
                break;
            default:
                canvas[rowPosition][columnPosition] = c;
                ++columnPosition;
                break;
        }
    }
}

void Canvas::refreshCanvas() {
    int numberOfRows = canvas.size()-1;
    
    for (int i = 1; i < numberOfRows; i++) {
        int rowSize = canvas[i].size()-1;
        
        for (int j = 1; j < rowSize; j++) {
            canvas[i][j] = ' ';
        }
    }
}

vector<vector<char>>* Canvas :: getCanvas() {
    return &canvas;
}



