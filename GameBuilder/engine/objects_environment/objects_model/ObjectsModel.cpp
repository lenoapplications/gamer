//
//  ObjectsModel.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 04/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include <stdio.h>
#include <string>
#include "ObjectModel.h"



int ObjectModel::getColumnPosition(){
    return columnPosition;
}
int ObjectModel::getRowPosition(){
    return rowPosition;
}
void ObjectModel::setDraw(std::string draw){
    objectDraw = draw;
}
std::string ObjectModel::getDraw(){
    return objectDraw;
}
void ObjectModel::setNewPosition(int x, int y){
    rowPosition = x;
    columnPosition = y;
}

