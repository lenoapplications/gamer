//
//  ObjectsEnvironment.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 04/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include <stdio.h>
#include "ObjectsEnvironment.h"



ObjectsEnvironment::~ObjectsEnvironment(){
    for (auto const& it : objects){
        ObjectModel* model = it.second;
        delete model;
    }
}

void ObjectsEnvironment::addObject(std::string objectName,ObjectModel* objectModel){
    objects[objectName] = objectModel;
}
void ObjectsEnvironment::removeObject(std::string objectName){
    objects.erase(objectName);
}
int ObjectsEnvironment::numberOfObjects(){
    return objects.size();
}
ObjectModel* ObjectsEnvironment::getObjectModel(std::string objectName){
    return objects.at(objectName);
}
std::map<std::string, ObjectModel*>& ObjectsEnvironment::getObjects(){
    return objects;
}
