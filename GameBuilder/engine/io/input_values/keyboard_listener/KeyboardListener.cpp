//
//  KeyboardListener.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 26/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include <stdio.h>
#include "KeyboardListener.h"

void KeyboardListener::initTerminalToUnblock(){
    /* get the terminal settings for stdin */
    tcgetattr(STDIN_FILENO,&old_tio);
    
    /* we want to keep the old setting to restore them a the end */
    new_tio=old_tio;
    
    /* disable canonical mode (buffered i/o) and local echo */
    new_tio.c_lflag &=(~ICANON & ~ECHO);
    
    /* set the new settings immediately */
    tcsetattr(STDIN_FILENO,TCSANOW,&new_tio);
}

void KeyboardListener::setTerminalToBlock(){
    tcsetattr(STDIN_FILENO,TCSANOW,&old_tio);
}

void KeyboardListener::listenForKeys(ObjectsEnvironment* objectsEnv){
    std::thread t1(&KeyboardListener::listenLoop,this,objectsEnv);
    t1.detach();
}

void KeyboardListener::listenLoop(ObjectsEnvironment* objectsEnv){
    do{
        c=getchar();
        inputProcessor->keyPressedProcessor(c, objectsEnv);
    } while(*ENGINE_RUNNING);
    
}


