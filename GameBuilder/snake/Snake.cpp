//
//  Snake.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 08/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#include <stdio.h>
#include "Snake.h"
#include <iostream>


Snake::Snake(){
    Engine::setInputProcessor(this);
    Engine::setObjectUpdater(this);
    initObjects();
    randomFoodPositionGenerator.seed(std::random_device()());
}


void Snake::objectUpdate(ObjectsEnvironment *objectsEnv){
    ObjectModel* snake = objectsEnv->getObjectModel("snake");
    
    int currentPositionRow = snake->getRowPosition();
    int currentPositionColumn = snake->getColumnPosition();
    
    switch (lastPressed) {
        case 'A':
            snake->setNewPosition(--currentPositionRow,currentPositionColumn);
            break;
        case 'B':
            snake->setNewPosition(++currentPositionRow, currentPositionColumn);
            break;
        case 'C':
            snake->setNewPosition(currentPositionRow, ++currentPositionColumn);
            break;
        case 'D':
            snake->setNewPosition(currentPositionRow, --currentPositionColumn);
            break;
    }
    checkFood(objectsEnv);
}

void Snake::checkFood(ObjectsEnvironment* objectsEnv){
    ObjectModel* snake = objectsEnv->getObjectModel("snake");
    ObjectModel* food = objectsEnv->getObjectModel("food");
    
    int snakeRow = snake->getRowPosition();
    int snakeColumn = snake->getColumnPosition();
    
    int foodRow = food ->getRowPosition();
    int foodColumn = food->getColumnPosition();
    
    if (snakeRow == foodRow && snakeColumn == foodColumn){
        std::uniform_int_distribution<int> positionRow(3,40);
        std::uniform_int_distribution<int> positionColumn(3,30);
        
        food->setNewPosition(positionRow(randomFoodPositionGenerator),positionColumn(randomFoodPositionGenerator));
        
        string currentSnakeDraw = snake->getDraw();
        currentSnakeDraw+="0";
        snake->setDraw(currentSnakeDraw);
    }
}


void Snake::keyPressedProcessor(char value, ObjectsEnvironment *objectsEnv){    
    switch (value) {
        case 'A':
            lastPressed = 'A';
            animationActive = true;
            break;
        case 'B':
            animationActive = true;
            lastPressed = 'B';
            break;
        case 'C':
            animationActive = true;
            lastPressed = 'C';
            break;
        case 'D':
            animationActive = true;
            lastPressed = 'D';
            break;
    }
}


void Snake::initObjects(){
    ObjectModel* snake = new ObjectModel(5,5);
    ObjectModel* food = new ObjectModel(20,20);
    snake->setDraw("x");
    food->setDraw("o");
    objectsEnvironment->addObject("food", food);
    objectsEnvironment->addObject("snake", snake);
}
