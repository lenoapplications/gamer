//
//  Snake.h
//  GameBuilder
//
//  Created by Filip Ćaćić on 08/08/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#pragma once
#include "Engine.h"
#include "InputProcessor.h"
#include "ObjectUpdater.h"
#include <random>

class Snake : public Engine,public InputProcessor,public ObjectUpdater{
private:
    char lastPressed = 'C';
    bool animationActive = false;
    std::mt19937 randomFoodPositionGenerator;
public:
    void keyPressedProcessor(char value,ObjectsEnvironment* objectsEnv);
    void objectUpdate(ObjectsEnvironment* objectsEnv);
    void checkFood(ObjectsEnvironment* objectsEnv);
    void createNewTail(ObjectsEnvironment* objectsEnv);
    void animations(ObjectModel* objectModel);
    void initObjects();
    Snake();
    ~Snake(){};
};
