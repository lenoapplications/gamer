//
//  main.cpp
//  GameBuilder
//
//  Created by Filip Ćaćić on 25/07/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include<string>
#include<thread>
#include"ScreenConfiguration.h"
#include"VisualConfigurer.h"
#include"Canvas.h"
#include "ObjectsEnvironment.h"
#include"Drawer.h"
#include <termios.h>
#include "KeyboardListener.h"
#include "Engine.h"
#include "InputProcessor.h"
#include "ObjectUpdater.h"
#include "Snake.h"
using namespace std;

void drawX(int i);
void readingKeys();
void mainFunction();
void snakeGame();



class B : public ScreenConfigurationModel {
    
public:
    B(int a, int b, int c, int d) : ScreenConfigurationModel(a, b, c, d) {}
    ~B() {
        cout << "deleting from B" << endl;
    }
};

class Processor : public InputProcessor {
public:
    Processor(){};
    ~Processor(){};
    void keyPressedProcessor(char value,ObjectsEnvironment* objectsEnv);
};

void Processor::keyPressedProcessor(char value,ObjectsEnvironment* objectsEnv){
    ObjectModel* objectModel = objectsEnv->getObjectModel("test");
    objectModel->setNewPosition(10, 10);
}

class Updater : public ObjectUpdater{
private:
public:
    Updater(){};
    ~Updater(){};
    void objectUpdate(ObjectsEnvironment* objectsEnv);
};

void Updater::objectUpdate(ObjectsEnvironment *objectsEnv){
    ObjectModel* testModel = objectsEnv->getObjectModel("test");
    ObjectModel* testModell = objectsEnv->getObjectModel("testt");
    
    int y = testModel->getColumnPosition();
    if (y > 75){
        y = 5;
    }
    testModel->setNewPosition(testModel->getRowPosition(), ++y);
}

int main()
{
    snakeGame();
    return 0;
}

void snakeGame(){
    Snake a{};
    a.setupDrawer(new ScreenConfigurationModel{80,160,5,55});
    a.startEngine();
}


void mainFunction() {
    
    Engine engine{};
    
    engine.setInputProcessor(new Processor());
    engine.setObjectUpdater(new Updater());
    
    ObjectsEnvironment* objectsEnv = engine.getObjectEnvironment();
    
    ObjectModel* x = new ObjectModel(5,5);
    ObjectModel* y = new ObjectModel(1,1);
    
    y->setDraw("c");
    
    x->setDraw("x");
    
    objectsEnv->addObject("test", x);
    objectsEnv->addObject("testt", y);
    
    engine.setupDrawer( new ScreenConfigurationModel{80,160,5,85} );
    
    engine.startEngine();
}

void drawX(int i) {
    char* spaces = new char[i];
}
//void readingKeys() {
//    KeyboardListener k{};
//    k.initTerminalToUnblock();
//    k.listenForKeys();
//    while(true){
//
//    }



